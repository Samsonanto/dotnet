using System;

namespace Adv
{

    /*
        Lambda is defined by 
        () => {}
        (x,y,z) => ...
        x => ...
        We  can assign a lambda exp to a deligate 
    
    */
    class Lamabda {

        public static void Sample(){

            Func<int,int> func = n => n*n;
            Action<int> func2 =  n => Console.WriteLine(n);
            Predicate<int> func3 = n => n == 1;

            Console.WriteLine(func(2)+" : "+func3(1) );
            func2(10);

        }

    }
}
