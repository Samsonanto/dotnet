using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace Adv
{
    class AsyncSample
    {

        public static async Task Method(){
            
            
            var res = await CostlyOps("https://www.google.com/");

            Console.WriteLine(res);
        
        
        } 
            

        private static async Task<string> CostlyOps(string url)
        {

            var webClient = new WebClient();
            var html = await webClient.DownloadStringTaskAsync(url);
            return "Done Async";

        }
    }
}
