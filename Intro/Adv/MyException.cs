using System;

namespace Adv
{
    class MyException :Exception
    {
        public MyException(string msg) : base(msg){}
        // public MyException(string msg,Exception ex) : base(msg,ex){}
        
    }
}
