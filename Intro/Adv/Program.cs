﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Adv
{
    class Program
    {
        static void Main(string[] args)
        {
            Generics<User> g = new Generics<User>();

            Delegate.Function func = Inc;
            //Keep adding new function so that it gets called and u need to change any code in the 
            //Implemented class 
            //Eg. : func += Inc;
            Delegate.Process((new Random()).Next(1, 1000), func);


            Lamabda.Sample();

            //Virtual Method
/*
            double distance, hour, fuel = 0.0;
            Console.WriteLine("Enter the Distance");
            distance = Double.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Hours");
            hour = Double.Parse(Console.ReadLine());
            Console.WriteLine("Enter the Fuel");
            fuel = Double.Parse(Console.ReadLine());
            Car objCar = new Car(distance, hour, fuel);
            Vehicle objVeh = objCar;
            objCar.Average();
            objVeh.Average();
            objCar.Speed();
            objVeh.Speed();
            Console.Read();
*/

            //Event and Delegate 

            Video video = new Video("Video 1");
            var videoEncoder = new VideoEncoder();
            var mailService = new MailService();
            var messageService = new MessageService();

            videoEncoder.VideoEncoded  += mailService.OnVideoEncoded;
            //In future we can add functionality to sent message
            videoEncoder.VideoEncoded += messageService.OnVideoEncoded; 


            videoEncoder.Encode(video);


            //Extention methods 

            string s = "Sample string";
            System.Console.WriteLine("String : "+s);

            s = s.Extention();

            System.Console.WriteLine("New String : "+s);


            //Linq is like java steams
            List<int> l =new List<int>(){1,2,3,4,5,6,7,8,9,10,11,12,13};


            var result = l.Where(x=>x<10).OrderByDescending(x=>x);
            System.Console.WriteLine(l.ToString());


            result = 
            from x in l 
            where x < 10
            orderby x descending
            select x;

            foreach (var item in result.ToArray())
            {
                Console.WriteLine(item);
            }
            /*
                Single(Predicate) => return a single obj that matchs the predicate*
                SingleOrDefualt(Predicate) => return a single obj that matchs the predicate else null
                First(Predicate) => first book that matchs the predicate*
                FirstOrDefault(Predicate) => Returns first or default value as null.
                Last and LstOrDefault (Predicate)
                Skip(n) =>  will skip first n entry of the list.
                Take(n) => will return first n entry of the list
                Aggregater functions like max,min,count,sum,average

                * Throws an exception if not found

             */ 


            //Nullable value types cannot be nullable
            // DateTime date = null; //It will give an error
            DateTime? date = null; //Nullable value type
            Nullable<DateTime> date1 = null; //Other way of doing the same
            //The Value prop should only be accessed if is set else it will give an error
            //U cannot assign nullable type to a value type 
            // DateTime d = date; //Will give an error
            DateTime d = date.GetValueOrDefault(); //This is the right way to do the same
            DateTime d2 = d; // this will not give an error
            DateTime d3 = date ?? DateTime.Now;
            /*
                The above code same as the following

                if(date != null)
                    d3 = date.GetValueOrDefault();
                else d3 = DateTime.Now;
            
             */

            //makes it similar like python or js 
            dynamic text = "Samson";
            text = 10 ;
            Console.WriteLine(++text);

            Console.WriteLine("Calling async method");
            AsyncSample.Method();
            Console.WriteLine("Excuting Programs after async call ...");

            while(true){}
        }

        static void Inc(int a)
        {
            Console.WriteLine(a + 1);
        }
    }
}
