using System;

namespace Adv
{
    class Generics <T> where T : User // This will restric the gentic type to User and its sub class
    {
        public void print(T value){
            Console.WriteLine(value.ToString());
        }

    }
}
