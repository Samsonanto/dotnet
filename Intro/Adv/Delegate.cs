namespace Adv
{
    /*
        They are function pointer which is used in create a framwork 
        also it make your code extencible.
        There are to in-build Deligate 
        Func <param,return> 
        Action<Param> 
     */

    class Delegate {
        
        public  delegate void Function(int a);

        public static void Process(int a, Function func){
            
            func(a);

        }


    }
}
