using System;
using System.Threading;

namespace Adv
{

    class VideoEncoder {

        // User defined deligate for c# < 2.x
        // public delegate void VideoEncoderHandler(object source,VideoEventArgs args);

        // We can use inbuild delegate called to EventHndler for version >2
        public event EventHandler<VideoEventArgs> VideoEncoded;  
        // public event EventHandler VideoEncoded // If u dont want to pass any args



        public void Encode(Video video){
            
            Console.WriteLine("Encoding Video... ");
            // Thread.Sleep(3000);
            OnVideoEncoded(video);

        }
        protected virtual void  OnVideoEncoded(Video video)
        {

            if(VideoEncoded != null){
                VideoEncoded(this,new VideoEventArgs(video));
            }

        }
    }
}
