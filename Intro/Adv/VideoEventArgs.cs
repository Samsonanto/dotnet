namespace Adv
{
    class VideoEventArgs
    {
        public Video Video { get; set; }
        public VideoEventArgs(Video video)
        {
            this.Video = video;
        }
    }
}
