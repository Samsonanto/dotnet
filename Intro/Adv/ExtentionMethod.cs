namespace Adv
{
    public static class ExtentionMethod
    {
        /*
            Extention is used for sealed class to add method to the same.
            Syntax will be same as in this example.

         */
        public static string Extention(this string str){
            return str+" Extention";
        }
    }
}
