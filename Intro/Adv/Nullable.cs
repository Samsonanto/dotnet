namespace Adv
{
    /*
    
        In C# value type (like int double etc ) cannot be null so by using Nullable generics class
        we give them this ability.

        Constraint : 
        where T : struct // value type
        where T : <Interface> // implements the interface
        where T : class // ref type 
        where T : new() //has a default ctor
        where T : <class> // class or sub class type.


   */

    class Nullable<T> where T : struct
    {
        private object _value;

        public Nullable(){}
        public Nullable(T value)
        {
            _value= value;
        }

        public bool HasValue
        {
            get { return _value != null ; }
        }


        public T GetValueOrDefault(){

            if(HasValue)
                return (T)_value;
            
            return default(T);
        }

    }
}
