using System;

namespace Inter
{
    /*
        Like Java one class can have only one parent class
    */
    class Inheritence {
        private int _f1;
        
        public int F2;

        public void PrintF1(){
            Console.WriteLine(_f1);
        }

        public Inheritence(int f1, int f2)
        {
            this._f1 = f1;
            this.F2 = f2;
        }
    }

    class Inherited : Inheritence {

// This wont Work
        // public Inherited()
        // {
        //    Console.WriteLine("Default"); 
        // }
        public Inherited(int f1, int f2) : base(f1,f2) 
        {
        }

    }
}
