using System;
using System.Collections.Generic;

namespace Inter
{
    //Readonly feild
    class ReadOnlySL{

        public static readonly List<int> l =new List<int>();

        public ReadOnlySL(){}

        void Reassign(){
            // l =new List<int>(); Not possible since its readonly also it is static
        }

         public void   addList(){
            l.Add( (new Random()).Next(1,1000) ); 
        }


        public List<int> getList () => l;  

    }
}
