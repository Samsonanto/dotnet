using System;

namespace Inter
{
    class StackOverFlowPost
    {
        /*
        Importent : Only if there is a set method we can update the property 
        else it will give a warning saying that the property is read only.
        */
        public int UpVote { get; private set; }

        public int DownVote { get; private set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; private set; }

        public StackOverFlowPost()
        {
            this.CreateDate = DateTime.Today;
            this.UpVote = 0;
            this.DownVote = 0;
        }

        public void UpVoted(){
            UpVote = UpVote + 1;
        }

        public void DownVoted(){
            DownVote = DownVote + 1;
        }
        
    }
}
