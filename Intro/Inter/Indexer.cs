using System.Collections.Generic;

namespace Inter
{
    partial class Indexer
    {
        /*
        ->  indexer can be used access a map the way we do it in JS
            eg : List<int> l = new List<int> ()
            l[0] = 1
                or 
            var map = new Dictionary<string,int>()
            map["Samson"] = 21;  
        -> It is an improvement over having an method for setting and getting from an index
         */

        private readonly Dictionary<string,string> _dictionary;

        public Indexer()
        {
            _dictionary = new Dictionary<string, string>();
        }


        //Creating custom indexer which will be class level that is now we can access the value in the class by indexer['x'];
        public string this[string key]{
            set { _dictionary[key] = value; }
            get { return _dictionary[key]; }
        }
    }
}
