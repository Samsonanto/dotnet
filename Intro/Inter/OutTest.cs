namespace Inter
{
    //Out, Basicly we can return multple values using out keyword 
    class OutTest {

        public int Number { get; set; }

        public OutTest(int i)
        {
            this.Number = i;
        }

        public static int testingOut(out int a){
            
            a=10;
            
            a = a+1;

            return 10;

        }

    }
}
