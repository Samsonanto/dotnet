namespace Inter
{
    //Composition is having one class object in another class

    /*
        Compostion over inheritance 
        eg. 
        Composition : 
        Person <- Animal method (eat,sleep)
        Dog <- Animal method (eat,sleep)
        Fish <- Animal method (eat,sleep)
        Person <- Walkable method (walk)
        Dog <- Walkable method (walk)
        Fish <- Swimable method (swim)

        Inheritance : 
        Person -> Animal method (eat,sleep)
        Dog -> Animal method (eat,sleep)
        Fish -> Animal method (eat,sleep)
        Now if we need a new method walk for Person and Dog then we will have to change hierarchy
        the inheritance.
        As we can see that Comopstion is less coupled we prefer compostion over inheritance.
     */

    class Composition {

        private Inherited _inherited;
        public Composition(Inherited i)
        {
            this._inherited = i;
        }
    }
}
