﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Inter
{
    class Program
    {

        static void PrintList(List<int> l){

            Console.WriteLine("Printing List");
            l.ForEach( e=> Console.WriteLine(e) );
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // Out

            List<int> l =new List<int>();
            
            l.Add(1);
            l.Add(2);
            l.Add(3);

            PrintList(l);


            int a = 1;
            int b;
            b = OutTest.testingOut(out a);
            Console.WriteLine("Values A : {0} B : {1} ",a,b);


            // ReadOnly 

            ReadOnlySL r1 = new ReadOnlySL();
            ReadOnlySL r2 = new ReadOnlySL();

            r1.addList();
            r1.addList();
            r1.addList();
            r1.addList();
            r1.addList();
            Console.WriteLine("After R1");
            PrintList(r1.getList());


            r2.addList();
            r2.addList();
            r2.addList();
            r2.addList();
            
            Console.WriteLine("After R2");
            PrintList(r2.getList());

            ReadOnlySL r3 = new ReadOnlySL();

            PrintList(r3.getList());

            // Access Specifier and props

            AccessSL sl = new AccessSL(new DateTime(1997,11,07));
            Console.WriteLine("My age is  "+sl.Age);


            //Indexer 

            Indexer i =new Indexer();
            i["name"] = "Samson";

            Console.WriteLine("Name : {0}",i["name"]);

            //Ex
            StackOverFlowPost post = new StackOverFlowPost();


            // Inheritance 

            Inherited inherited = new Inherited(1,2);
            //All visible props and methods from the child class
            inherited.F2 = 2;
            inherited.PrintF1();


            /* Upcasting and down casting 

                Parent p = new Child(); up
                Child c = (Child)p; down

                is boolean o/p weather a given obj is of a perticula type 
                if(p is Child)    //true

                as returns null if there is a type miss match 

                Child c = p as Child

                if(c == null ) { ... }            

            */    

            ArrayList array = new ArrayList();

            array.Add(2);
            array.Add("sdf");

            var x = array[0];

            Console.WriteLine(x);

            x = array[1];

            Console.WriteLine(x);

            //Boxing and UnBoxing aviod it as it has a performance penalty

            //Abstract same as java, Virtual is a empty method that may or may not be implemented in child class        

            //Sealed Class is like final class in java
            
        }   

    }
}
