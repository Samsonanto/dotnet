using System;

namespace Inter
{
    /*
        Acess-specifier  : 
        -> Private : Invisible to the out side world (Private)
        -> Public : Visible to the out side world (Public)
        -> Protected : Visible by derived class
        -> Internal : Visible by same namespace (default)
        -> Protected Internal :  same as protected in java 
    */

    class AccessSL{
        
        /*
            -> Method and field should be PascalCase for class
            ->Method parms should be camalCase
            -> Private field should be _camelCase
            -> Properties with out logic should be above ctor and with logic should be below ctor
         */


        public string Name { get; set; }
        public DateTime Birthdate { get; private set; }
        // if uwant the birthdate to be set only once then create the set method to be private and initialize the same in ctor
         public int TestProps { get; set; } //the CLR creates a field for the same during complie time
        // Other implementation is given below
        private int _testProps2 ; //private fields uses _camelCase
        public int TestProps2 {

            get {return _testProps2; }
            set { _testProps2 = value; }

        }
        public AccessSL(DateTime Birthdate)
        {
            this.Birthdate = Birthdate;
        }
        //We can have custome logic in the props
        public int Age {

            get {

                int timeSpan = (DateTime.Today - Birthdate).Days;
                int year = timeSpan/365;
                return year;

            }

        }

    }
}
