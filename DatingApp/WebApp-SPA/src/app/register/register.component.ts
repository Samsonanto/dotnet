import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../_service/auth.service';
import { AlertifyService } from '../_service/alertify.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  model: any = {};

  constructor(
    private authRegister: AuthService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {}

  register() {
    this.authRegister.register(this.model).subscribe(
      res => {
        this.alertify.success('Registered Successfully');
      },
      err => {
        this.alertify.error(err);
      }
    );
  }

  cancel() {
    this.cancelRegister.emit(false);
    console.log('Cancelled');
  }
}
