import { Injectable } from '@angular/core';
import * as alertify from 'alertifyjs';

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {
  constructor() {}
  confirm(messages: string, okCallback: () => any) {
    alertify.confirm(messages, (e: any) => {
      if (e) {
        okCallback();
      } else {
      }
    });
  }

  success(messages) {
    alertify.success(messages);
  }
  error(messages) {
    alertify.error(messages);
  }
  warning(messages) {
    alertify.warning(messages);
  }
  message(messages) {
    alertify.message(messages);
  }
}
