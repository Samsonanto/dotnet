import { Component, OnInit } from '@angular/core';
import 'bootstrap/dist/css/bootstrap.min.css';
import { AuthService } from '../_service/auth.service';
import { AlertifyService } from '../_service/alertify.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {};
  username: string;

  constructor(
    private authService: AuthService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.username = localStorage.getItem('name');
  }

  login() {
    this.authService.login(this.model).subscribe(
      next => {
        this.alertify.success('Login Sucessfull');
      },
      err => {
        this.alertify.error(err);
        console.log(err);
      }
    );
  }

  loggedIn() {
    return this.authService.isLoggedIn();
  }
  logOut() {
    localStorage.removeItem('token');
    this.alertify.success('Logged Out succesfully');
  }
}
