using System.ComponentModel.DataAnnotations;

namespace WebAPI.API.Dtos
{
    public class UserRegisterDTO
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(8,MinimumLength =4, ErrorMessage = "Password must be of length between 4 and 8")]
        public string Password { get; set; }
    }
}