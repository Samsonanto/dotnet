namespace WebAPI.API.Dtos
{
    public class UserLoginDTO
    {
        public string Password { get; set; }
        public string Username { get; set; }
    }
}