using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebAPI.API.Model;

namespace WebAPI.API.Data
{
    public class AuthRepository : IAuthRepository
    {
        private readonly DataContext _context;

        public AuthRepository(DataContext context)
        {
            _context = context;
        }
        public async Task<User> Login(string username, string password)
        {
            var user =  await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

            if(user != null && VarifyPassword(password,user.PasswordHash,user.PasswordSalt))
                return user;
            
            return null;


        }

        private bool VarifyPassword(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt)){    

                byte[] computeHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

                for(int i=0 ; i<computeHash.Length;i++){
                    if(computeHash[i] != passwordHash[i]) 
                        return false;
                }


            }
            return true;

        }

        public async Task<User> Register(User user, string password)
        {
            byte[] passwordHash,passwordSalt;

            CreatePasswordHash(password,out passwordHash,out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            await _context.AddAsync(user);
            await _context.SaveChangesAsync();

            return user;
            

        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using(var hmac = new System.Security.Cryptography.HMACSHA512()){    

                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));

            }

        }

        public async Task<bool> UserExist(string username)
        {

            return  await _context.Users.AnyAsync(x => x.Username == username );

        }
    }
}