﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebAPI.API.Data;

namespace WebAPI.API.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly DataContext _context;

        public WeatherForecastController(DataContext context)
        {
            _context = context;
        }

        [AllowAnonymous]        
        [HttpGet]
        public async Task<IActionResult> Values()
        {
            var values = await _context.Values.ToListAsync();
            // throw new Exception("Test Exception"); will show exeption page if in devlopment mode
            Console.WriteLine(values[0]);

            return Ok(values);

        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> Values(string id){

            if(Int32.TryParse(id,out int res)){
                var value =  await _context.Values.FirstOrDefaultAsync(x => x.Id==res);
                Console.WriteLine("Next");
                if(value == null){
                    return NotFound("ID Not Found");
                }

                return Ok(value);
            }
            return BadRequest("Invalid Id");
        }
    }
}
