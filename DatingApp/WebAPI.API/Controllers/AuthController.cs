using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using WebAPI.API.Data;
using WebAPI.API.Dtos;
using WebAPI.API.Model;
using System.Text;
using System;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace WebAPI.API.Controllers
{
    [ApiController]
    //The apicontroller is responsible for checking the modelstate that is if it is a valid input from the 
    //Frontend and also provide main other features like mapping the dtos to the incoming data.
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _repo;
        private readonly IConfiguration  _config;

        public AuthController(IAuthRepository repo, IConfiguration config )
        {
            _repo = repo;
            _config = config;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegisterDTO user){

            user.Username = user.Username.ToLower();
            if(await _repo.UserExist(user.Username)){
                return BadRequest("Username already taken");
            }
            var newUser = new User(){ Username = user.Username };
            await _repo.Register(newUser,user.Password);

            // return Ok("User created sucessfully");
            return StatusCode(201);

        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserLoginDTO userLoginDTO){

            /*
                JWT Consist of 
                Header : typ : JWT , Algo : hs512
                Playload : Claim like(Username,id etc)// carefull it can be viewed by others 
                Also the iat,nbf and exp.
                secret : it cannot be seen by user and is used by the Server to validate the token

                Creating the token has three steps 
                ->Creds
                ->Claims
                ->Other Info
            
             */   
            
            var user = await _repo.Login(userLoginDTO.Username.ToLower(),userLoginDTO.Password);

            if(user == null)
                return Unauthorized("Unauthorized");
            
            //Create the claims 
            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier,user.Id.ToString()),
                new Claim(ClaimTypes.Name,user.Username)
            };

            //Create the Key

            var key =  new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("Appsettings:Token").Value));

            var creds =  new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            var tokenDiscriptor = new SecurityTokenDescriptor {
               
                Subject  = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds

            };
            var tokenHandler = new JwtSecurityTokenHandler(); 
            var token = tokenHandler.CreateToken(tokenDiscriptor);
            return Ok(new {
                Token = tokenHandler.WriteToken(token)
            });

        }
    }
}