namespace Demo.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DataContextOptions<DataContext> options) : base (options){}

        public Dbset<Value> Values { get; set; }
    }

}